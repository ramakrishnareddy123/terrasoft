import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-forms-page',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormsComponent implements OnInit {

  accountDetailsForm: FormGroup;  

  account_validation_messages = {
    'firstname': [
      { type: 'required', message: 'First name is required' }
    ],
    'lastname': [
      { type: 'required', message: 'Last name is required' }
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'phonenumber': [
      { type: 'required', message: 'Phone number is required' }
    ],
  }
  userdata: any;
  first_name: any;
  last_name: any;
  usr_number: any;
  usr_email: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.accountDetailsForm = this.fb.group({
      firstname: new FormControl('', Validators.compose([
       Validators.required
      ])),
      lastname: new FormControl('', Validators.compose([
        Validators.required
       ])),
      email: new FormControl('', Validators.compose([
        Validators.required
      ])),
      phonenumber: new FormControl('', Validators.compose([
        Validators.required
       ])),
    })
  }

  onSubmitAccountDetails(value){
    this.userdata = value;
    this.first_name = value.firstname;
    this.last_name = value.lastname;
    this.usr_email = value.email;
    this.usr_number = value.phonenumber;

    console.log('Form Vals', value);
  }

  onSubmitUserDetails(value){
    console.log(value);
  }

}
